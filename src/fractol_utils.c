/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/04 12:10:34 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/07/06 16:03:26 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fractol.h"

void	my_pixel(t_data *data, int x, int y, double color)
{
	char	*dst;

	dst = data->addr + (y * data->line_length + x * (data->bits_per_pixel / 8));
	*(unsigned int *) dst = color;
}

int	rgbtoint(int red, int green, int blue)
{
	red = (red << 16);
	green = (green << 8);
	return (red | green | blue);
}

void	create_color_palette(t_rgb_color palette[])
{
	int		i;
	float	t;

	i = 0;
	while (i < MAX_ITERATIONS)
	{
		t = (float) i / MAX_ITERATIONS;
		palette[i].red = (int)(t * 644);
		palette[i].green = (int)(t * 474);
		palette[i].blue = (int)(t * 98);
		i++;
	}
}

int	close_window(void)
{
	exit(0);
}

int	key_handle(int keycode)
{
	if (keycode == 53)
		close_window();
	return (0);
}
