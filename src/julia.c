/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/23 16:14:50 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/07/06 16:26:19 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fractol.h"

void	assign_color(t_vars *vars, int iter, int x, int y)
{
	if (iter == -1)
		*(unsigned int *)(vars->data->addr
				+ (y * vars->data->line_length)
				+ (x * (vars->data->bits_per_pixel / 8))) = 0x000000;
	else
		*(unsigned int *)(vars->data->addr + (y * vars->data->line_length)
				+ (x * (vars->data->bits_per_pixel / 8)))
			= rgbtoint(vars->palette[iter].red,
				vars->palette[iter].green, vars->palette[iter].blue);
}

int	julia_set(int x, int y, float zoom, t_complex c)
{
	t_complex	z;
	int			iter;
	double		temp;

	z.real = 1.5 * (x - WIDTH / 2.0) / (0.5 * zoom * WIDTH);
	z.imaginary = (y - HEIGHT / 2.0) / (0.5 * zoom * HEIGHT);
	iter = 0;
	while (z.real * z.real + z.imaginary
		* z.imaginary < 4 && iter < MAX_ITERATIONS)
	{
		temp = z.real * z.real - z.imaginary * z.imaginary + c.real;
		z.imaginary = 2 * z.real * z.imaginary + c.imaginary;
		z.real = temp;
		iter++;
	}
	if (iter >= MAX_ITERATIONS)
		return (-1);
	return (iter);
}

void	draw_julia(t_vars *vars)
{
	int			x;
	int			y;
	int			iter;

	y = 0;
	while (y < HEIGHT)
	{
		x = 0;
		while (x < WIDTH)
		{
			iter = julia_set(x, y, vars->zoom, vars->c);
			assign_color(vars, iter, x, y);
			x++;
		}
		y++;
	}
	mlx_put_image_to_window(vars->mlx, vars->win, vars->data->img, 0, 0);
}

int	mouse_event_julia(int button, int x, int y, t_vars *vars)
{
	x = 0;
	y = 0;
	if (button == 5)
		vars->zoom *= 1.1;
	else if (button == 4)
		vars->zoom /= 1.1;
	draw_julia(vars);
	return (0);
}

void	julia(double real, double imaginary_value)
{
	t_data		data;
	t_vars		vars;

	create_color_palette(vars.palette);
	vars.mlx = mlx_init();
	vars.zoom = 0.7;
	vars.win = mlx_new_window(vars.mlx, WIDTH, HEIGHT, "Julia Set");
	data.img = mlx_new_image(vars.mlx, WIDTH, HEIGHT);
	data.addr = mlx_get_data_addr(data.img,
			&data.bits_per_pixel, &data.line_length, &data.endian);
	vars.data = &data;
	vars.c.real = real;
	vars.c.imaginary = imaginary_value;
	draw_julia(&vars);
	mlx_mouse_hook(vars.win, &mouse_event_julia, &vars);
	mlx_hook(vars.win, 2, 1L << 0, &key_handle, &vars);
	mlx_hook(vars.win, 17, 0L, &close_window, &vars);
	mlx_loop(vars.mlx);
}
