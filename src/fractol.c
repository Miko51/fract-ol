/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/23 15:58:10 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/07/06 17:39:54 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fractol.h"

void	ft_putstr(char *s)
{
	int		i;

	i = 0;
	while (s[i])
	{
		write(1, s + i, 1);
		i++;
	}
}

int	ft_strcmp(char *s1, char *s2)
{
	while (*s1 == *s2 && *s1)
	{
		s1++;
		s2++;
	}
	return (*s1 - *s2);
}

int	main(int argc, char **argv)
{
	if (argc == 1)
	{
		ft_putstr("Please enter a fractol set\n");
		ft_putstr("Defined arguments:\nmandelbrot\njulia-{1, 2, 3}\n");
	}
	else if (ft_strcmp(argv[1], "mandelbrot") == 0)
		mandelbrot();
	else if (ft_strcmp(argv[1], "julia-1") == 0)
		julia(0.355, 0.355);
	else if (ft_strcmp(argv[1], "julia-2") == 0)
		julia(-0.54, 0.54);
	else if (ft_strcmp(argv[1], "julia-3") == 0)
		julia(-0.4, -0.59);
	else
	{
		ft_putstr("Undefined argument\n");
		ft_putstr("Defined arguments:\nmandelbrot\njulia-{1, 2, 3}\n");
	}
	exit(0);
}
