/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/23 16:04:11 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/07/06 16:04:21 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fractol.h"
#include <stdio.h>

int	iteration(t_vars *vars, t_m *m, int x, int y)
{
	double			z_re2;
	double			z_im2;
	int				n;

	n = 0;
	while (n < MAX_ITERATIONS)
	{
		z_re2 = m->z_re * m->z_re;
		z_im2 = m->z_im * m->z_im;
		if (z_re2 + z_im2 > 4)
		{
			my_pixel(vars->data, x, y, rgbtoint(vars->palette[n].red,
					vars->palette[n].green, vars->palette[n].blue));
			break ;
		}
		m->z_im = 2 * m->z_re * m->z_im + m->c_im;
		m->z_re = z_re2 - z_im2 + m->c_re;
		n++;
	}
	return (n);
}

void	mandelbrotset(t_vars *vars, t_m *m)
{
	unsigned int	y;
	unsigned int	x;
	int				n;

	m->re_factor = (vars->max_re - vars->min_re) / (WIDTH - 1);
	m->im_factor = (vars->max_im - vars->min_im) / (HEIGHT - 1);
	y = 0;
	while (y < HEIGHT)
	{
		x = 0;
		m->c_im = vars->max_im - y * m->im_factor;
		while (x < WIDTH)
		{
			my_pixel(vars->data, x, y, 0x000000);
			m->c_re = vars->min_re + x * m->re_factor;
			m->z_re = m->c_re;
			m->z_im = m->c_im;
			n = iteration(vars, m, x, y);
			if (n == MAX_ITERATIONS)
				my_pixel(vars->data, x, y, 0x000000);
			x++;
		}
		y++;
	}
	mlx_put_image_to_window(vars->mlx, vars->win, vars->data->img, 0, 0);
}

void	handle_zoom(int button, int x, int y, t_vars *vars)
{
	t_m		factor;
	double	center_x;
	double	center_y;
	double	new_width;
	double	new_height;

	new_width = 0;
	new_height = 0;
	center_x = vars->min_re + (vars->max_re - vars->min_re) * x / WIDTH;
	center_y = vars->min_im + (vars->max_im - vars->min_im) * y / HEIGHT;
	if (button == 5)
	{
		new_width = (vars->max_re - vars->min_re) / 2.0;
		new_height = (vars->max_im - vars->min_im) / 2.0;
	}
	else if (button == 4)
	{
		new_width = (vars->max_re - vars->min_re) * 2.0;
		new_height = (vars->max_im - vars->min_im) * 2.0;
	}
	vars->min_re = center_x - new_width / 2;
	vars->min_im = center_y - new_height / 2;
	vars->max_re = center_x + new_width / 2;
	vars->max_im = center_y + new_height / 2;
	mandelbrotset(vars, &factor);
}

int	mouse_event(int button, int x, int y, t_vars *vars)
{
	if (button == 5 || button == 4)
		handle_zoom(button, x, y, vars);
	return (x);
}

void	mandelbrot(void)
{
	t_vars	vars;
	t_data	img;
	t_m		m;

	vars.mlx = mlx_init();
	vars.win = mlx_new_window(vars.mlx, WIDTH, HEIGHT, "MandelBrot");
	img.img = mlx_new_image(vars.mlx, WIDTH, HEIGHT);
	img.addr = mlx_get_data_addr(img.img, &img.bits_per_pixel,
			&img.line_length, &img.endian);
	vars.data = &img;
	vars.min_re = -2.0;
	vars.max_re = 1.0;
	vars.min_im = -1.2;
	vars.zoom = 1.0;
	vars.max_im = vars.min_im + (vars.max_re - vars.min_re)
		* HEIGHT / WIDTH;
	create_color_palette(vars.palette);
	mandelbrotset(&vars, &m);
	mlx_mouse_hook(vars.win, &mouse_event, &vars);
	mlx_hook(vars.win, 2, 1L << 0, &key_handle, 0);
	mlx_hook(vars.win, 17, 0L, &close_window, 0);
	mlx_loop(vars.mlx);
}
