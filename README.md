[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# Fract-ol
**Fractal shapes are drawn using the Minilibx library in the project.**

## Usage
```bash
git clone https://gitlab.com/Miko51/fract-ol.git && cd fract-ol
make && ./fractol <fractal set>
```

Arguments: [mandelbrot, julia-1, julia-2, julia-3] <br />
Additionally, you can exit with the ESC key and perform zoom in and zoom out using the mouse wheel.<br />

![](./img/screenshot.png)

![](./img/screenshot2.png)

![](./img/screenshot3.png)

## Score
100/125 [Project subject](en.subject.pdf)

## Sources
[This](https://gontjarow.github.io/MiniLibX/) resource will enable you to get started with the Minilibx library.<br />
[This](https://www.youtube.com/watch?v=wfOD3qdCIIw) video will help you understand the logic of fractals.<br />
[This](http://warp.povusers.org/Mandelbrot/)  document will help you understand how to draw a fractal using the C language.<br />
