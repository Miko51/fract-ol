CC = gcc
CFLAGS = -Wall -Wextra -Werror
NAME = fractol 
SRC_DIR = src
OBJ_DIR = obj
MLX = libmlx.dylib
RM = rm -rf
SRC := $(wildcard $(SRC_DIR)/*.c)
OBJ = $(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/%.o,$(SRC))

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	@mkdir -p obj
	$(CC) $(CFLAGS) -c  $< -o $@

all:  $(NAME)

$(MLX):
	cp miniLibX/libmlx.dylib .

$(NAME): $(OBJ) $(MLX)
	gcc -L ./miniLibX -lmlx -framework OpenGl -framework AppKit  $(OBJ) -o $(NAME)

clean:
	$(RM) $(OBJ)

fclean:	clean
	$(RM) $(NAME)
	$(RM) $(MLX)

re:	fclean all

.PHONY: all fclean clean re
