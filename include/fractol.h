/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/23 16:44:33 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/07/06 16:26:20 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include "../miniLibX11/mlx.h"
# define WIDTH 900
# define HEIGHT 900
# define MAX_ITERATIONS 100

typedef struct s_complex
{
	double	real;
	double	imaginary;
}				t_complex;

typedef struct s_data
{
	void	*img;
	char	*addr;
	int		bits_per_pixel;
	int		line_length;
	int		endian;
}	t_data;

typedef struct s_colors
{
	int	red;
	int	green;
	int	blue;
}	t_rgb_color;

typedef struct s_vars
{
	float			zoom;
	t_complex		c;
	void			*mlx;
	void			*win;
	t_data			*data;
	double			min_re;
	double			max_re;
	double			min_im;
	double			max_im;
	t_rgb_color		palette[100];
}				t_vars;

typedef struct s_mandel
{
	double	re_factor;
	double	im_factor;
	double	c_im;
	double	c_re;
	double	z_im;
	double	z_re;
}	t_m;

void	mandelbrot(void);
void	my_pixel(t_data *data, int x, int y, double color);
void	create_color_palette(t_rgb_color palette[]);
void	julia(double real, double imaginary_value);
int		close_window(void);
int		rgbtoint(int red, int green, int blue);
int		key_handle(int keycode);

#endif
