[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# Fract-ol
**Projede Minilibx kütüphanesi kullanılarak fractal şekilleri çizdirilir.**

## Kullanım
```bash
git clone https://gitlab.com/Miko51/fract-ol.git && cd fract-ol
make && ./fractol <fractal şekli>
```

Kurulum ve çalıştırma yukarıdaki gibidir. <br />
Argümanlar: [mandelbrot, julia-1, julia-2, julia-3] <br />
Ek olarak ESC tuşu ile çıkabilir ve fare tekerleği ile zoom in, zoom out yapabilirsiniz.<br />

![](./img/screenshot.png)

![](./img/screenshot2.png)

![](./img/screenshot3.png)

## Not
Proje %100 ile geçmektedir bonus dahil değildir. [Projenin yönergeleri](tr.subject.pdf)

## Faydalanabilinecek Kaynaklar
[Bu](https://gontjarow.github.io/MiniLibX/) kaynak Minilibx kütüphanesine giriş yapmanızı sağlayacaktır.<br />
[Bu](https://www.youtube.com/watch?v=wfOD3qdCIIw) video fraktalların mantığını anlamanızı sağlayacaktır.<br />
[Bu](http://warp.povusers.org/Mandelbrot/) döküman C dili ile bir fraktalın nasıl çizdirileceğini anlamanızı sağlayacaktır.<br />
